from django.contrib import admin
from .models import *


admin.site.register(User, admin.ModelAdmin)
admin.site.register(Post, admin.ModelAdmin)
admin.site.register(Organization, admin.ModelAdmin)
admin.site.register(UserOrgAssociation, admin.ModelAdmin)
