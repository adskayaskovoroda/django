from rest_framework import routers
from .views import UserViewSet, PostViewSet

router = routers.SimpleRouter()

router.register(r'users', UserViewSet)
router.register(r'posts', PostViewSet)

urlpatterns = router.urls
