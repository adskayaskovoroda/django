from django.db import models


class Post(models.Model):
    content = models.TextField()

    author = models.ForeignKey('User', on_delete=models.CASCADE)

    class Meta:
        permissions = [
            ('test_perm', 'Test Permission')
        ]
