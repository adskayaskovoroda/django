from django.db import models


class Organization(models.Model):
    name = models.CharField(max_length=100, null=False, unique=True)

    primary_contact = models.ForeignKey('User', on_delete=models.DO_NOTHING)

    def __str__(self):
        return self.name
