from django.db import models


class UserOrgAssociation(models.Model):
    user = models.ForeignKey('User', on_delete=models.DO_NOTHING)
    organization = models.ForeignKey('Organization', on_delete=models.DO_NOTHING)

    class Role(models.TextChoices):
        ADMIN = 'SU', 'Admin'
        USER = 'DU', 'User'

    role = models.CharField(max_length=2, choices=Role.choices, default=Role.USER)

    def __str__(self):
        return f'[{self.user}] - [{self.organization}] - [{self.role}]'
