from rest_framework import serializers
from web.models import UserOrgAssociation


class UserOrgAssociationSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserOrgAssociation
        fields = ['__all__']
